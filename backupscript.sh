#!/bin/bash
#	Created On: 23-May-2014
#	Author	  : OpenSCG PDO Team
#	Purpose   : Postgres Database backup
#	Version   : 1.3
# Setting default options,
# which need to be modify as per the customer environment
#
PGUSER="postgres"
PGPASSWORD="postgres"
PGDUMP=`which pg_dump`
PGPORT=5432

# Global declaration.
#

PGBKLOC=""
PGBKDB=""
PGBKHOST=""
PGBKRETN=2
PGBKMDE="compress"
PGBKUSR=$PGUSER
PGBKPRT=$PGPORT
PGBKFILEPRFX=`date +_%Y%m%d_%H%M%S`

# Email functionality
#
HTMLCNT="<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Strict//EN\"\"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd\">"
PGBKSENTMAIL="dineshkumar02@gmail.com,dinesh.chemuduru@openscg.com"
PGBKMAILSUBJECT="OpenSCG PDO's ACK: `date -dd`"
PGBKFILEDETAILS=""
PGBKRENTFILES=""


# Functions Declaration
#
usage()
{
cat << EOF
usage: $0 options

This postgres backup script require, below valid options. 

OPTIONS:
   -h      Show this message
   -l      Backup location
   -d      Source databases
   -H	   Source host
   -r	   Backup retention in days
   -m	   Backup mode (plain/compress)
   -u	   Source database username
   -p	   Source database port

Example:
	backupscript.sh -H localhost -d postgres,postgis -r 10 -m plain -u postgres -p 5432
	backupscript.sh	-H 192.168.214.10 -d postgres -m compress -u postgres

EOF
}

log_message() {
	echo "`date -dd` $1: $2"
}

check_pgdump_path() {
	if [ "x$PGDUMP" = "x" ]; then
		log_message "ERROR" "pg_dump binary not found";
		log_message "HINT" "Ensure you setup the pg_dump path in your PATH";
		exit 1;
	fi
}

get_options() {

	while getopts "hl:d:r:H:u:p:m:" OPTION
	do
		case $OPTION in
		h)
			usage
			exit 1
			;;
		l)
			PGBKLOC=$OPTARG
			;;
		d)
			PGBKDB=$OPTARG
			;;
		r)
			if [ $OPTARG -lt 0 ]; then
				log_message "ERROR" "Invalid no.of retention days: $OPTARG"
				log_message "HINT" "Minimum no.of retention days is 1"
				exit 1;
			fi
			PGBKRETN=$OPTARG 
			;;
		H)
			PGBKHOST=$OPTARG
			;;
		u)
			PGBKUSR=$OPTARG
			;;
		p)
			PGBKPRT=$OPTARG
			;;
		m)
			if [ "$OPTARG" != "compress" -a "$OPTARG" != "plain" ]; then
				log_message "ERROR" "Invalid backup mode: $OPTARG"
				log_message "HINT" "Provide plain/compress as an option"
				exit 1;
			fi
			PGBKMDE=$OPTARG
			;;
		?)
			usage
			exit
			;;
		esac
	done
}

check_options() {

	if [[ -z $PGBKDB ]] || [[ -z $PGBKLOC ]] || [[ -z $PGBKHOST ]]
	then
		log_message "ERROR" "Host, Backup location, Backup dbs are mandatory options"
		echo
		usage;
		exit 1;
	fi
}


start_backup() {

	export PGPASSWORD

	for dbname in $(echo $PGBKDB|tr "," " ")
	do
		BK_START_TIME=$(date +"%s")
		BK_STATUS="Success"
		log_message "NOTICE" "Starting backup of $dbname"
		CMD="$PGDUMP `[[ \"$PGBKMDE\" = \"compress\" ]] && echo \"-Fc -f $PGBKLOC/$dbname$PGBKFILEPRFX.pg_dump\" || echo \" -f $PGBKLOC/$dbname$PGBKFILEPRFX.sql\"` -h $PGBKHOST -U $PGBKUSR -p $PGBKPRT $dbname"
		echo $CMD;
		$CMD;
		if [ $? -ne 0 ]; then
			log_message "ERROR" "Backup failure due to some errors"
			BK_STATUS="Failed"
		fi
		log_message "NOTICE" "Completed backup of $dbname"
		BK_END_TIME=$(date +"%s")
		BK_DURATION=$(($BK_END_TIME - $BK_START_TIME));
		log_message "NOTICE" "Backup of $dbname took $(($BK_DURATION / 60)) minutes and $(($BK_DURATION % 60)) seconds"
		echo
		BK_FILE_SIZE_CMD="du -h `[[ \"$PGBKMDE\" = \"compress\" ]] && echo \"$PGBKLOC/$dbname$PGBKFILEPRFX.pg_dump\" || echo \"$PGBKLOC/$dbname$PGBKFILEPRFX.sql\"`"
		BK_FILE_SIZE=`$BK_FILE_SIZE_CMD|awk -F " " '{print \$1}'`
		#Storing the dump file details, for sending an e-mail.
		#
		PGBKFILEDETAILS+="$dbname#`date -d @$BK_START_TIME +"%Y-%m-%d&nbsp;%H:%M:%S"`#`date -d @$BK_END_TIME +"%Y-%m-%d&nbsp;%H:%M:%S"`#$(($BK_DURATION / 60))-Minutes#$BK_FILE_SIZE#$BK_STATUS@"
	done

}

start_retention() {
	
	i=0;
	log_message "NOTICE" "Starting file retention"
	
	for retenfile in `find $PGBKLOC -mtime +$PGBKRETN -type f -regex ".*.pg_dump\|.*.sql" -exec ls -f {} \;`
	do
		# Per a single row, put two files.
		# Otherwise, insert 5 spaces.
		#
		i=`expr $i + 1`
		PGBKRENTFILES+=$retenfile
		[[ $(($i%2)) != 0 ]] && PGBKRENTFILES+="&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" || PGBKRENTFILES+="<br/>"
		rm -vf $retenfile
	done

	log_message "NOTICE" "Complete file retention"
}

# HTML section for pretty dump e-mail.
#
begin_html_content() {
	HTMLCNT+=" <html xmlns=\"http://www.w3.org/1999/xhtml\" xml:lang=\"en\" lang=\"en\">";
}

open_html_tag() {
	# $1 is command, and $2 is the attribute with values.
	#
	HTMLCNT+=" <$1 $2>";
}

put_tag_text() {
	HTMLCNT+=" $1";
}

close_html_tag() {
	HTMLCNT+=" </$1>";
}

end_html_content() {
	HTMLCNT+=" </html>"
}

prepare_dumpfile_table_header() {
	
	Headers="Database@Start&nbsp;time@End&nbsp;time@Duration@Size@Status"
	open_html_tag "tr" "style=\"text-align: left;  background: #A9A9A9;font-weight:bold;font-size:13px;\""
		
	for head in $(echo $Headers|tr "@" " ")
	do
		open_html_tag "td" "style=\"border: 1px solid\""
		put_tag_text $head
		close_html_tag "td"
	done

	close_html_tag "tr"
}

prepare_dumpfile_details_table() {
	
	for dbdetails in $(echo $PGBKFILEDETAILS|tr "@" " ")
	do
		open_html_tag "tr" "style=\"border: 1px solid\""
		for content in $(echo $dbdetails|tr "#" " ")
		do
				open_html_tag "td" "style=\"border: 1px solid\""
				put_tag_text $content
				close_html_tag "td"
		done
		close_html_tag "tr"
	done
	
}

prepare_retention_deatils_table() {
	
	open_html_tag "tr" " "
		open_html_tag "td" "style=\"border: 1px solid; text-align: left;  background: #A9A9A9;font-weight:bold;font-size:13px;\""
			put_tag_text "Files"
		close_html_tag "td"
		
		open_html_tag "td" "style=\"border: 1px solid background: WHITE; font-weight:normal;\" colspan=5"
				put_tag_text $PGBKRENTFILES
		close_html_tag "td"
	close_html_tag "tr"
}
#main

check_pgdump_path;
get_options "$@";
check_options;
start_backup;
start_retention;

#Preparing HTML E-Mail
#
begin_html_content
	open_html_tag "body" ""
		open_html_tag "div" "style=\"border: 2px solid #000;border-radius: 5px;width:auto;\""
			open_html_tag "table" "style=\"width: 100%; border-collapse: collapse;\""

				# Table Header
				#
				open_html_tag "tr" "style=\"text-align: center; background: rgb(18, 52, 86); color: white; font-weight: bold; border-top-right-radius: 14px;\"";
					open_html_tag "td" "colspan=6"
						put_tag_text "OpenSCG PDO's ACK"
					close_html_tag "td"
				close_html_tag "tr"
				# Table Sub Header
				#
				open_html_tag "tr" "style=\"text-align: center; background: lightgrey; font-size: 12px; font-weight: bold;\""
					open_html_tag "td" "colspan=6"
						put_tag_text "Pg Dump Activity"
					close_html_tag "td"
				close_html_tag "tr"
				
				# Dump file details
				#
				prepare_dumpfile_table_header
				prepare_dumpfile_details_table
				
				#Retention files
				#
				open_html_tag "tr" "style=\"text-align: center; background: lightgrey; font-size: 12px; font-weight: bold;\""
					open_html_tag "td" "colspan=6"
						put_tag_text "Retention Files"
					close_html_tag "td"
				close_html_tag "tr"
				prepare_retention_deatils_table
				
			close_html_tag "table"
		close_html_tag "div"
	close_html_tag "body"
end_html_content

echo $HTMLCNT | mail -a "Content-type: text/html" -s "$PGBKMAILSUBJECT" $PGBKSENTMAIL
